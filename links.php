

<!DOCTYPE html>
<html lang="en">
<? include("admin/includes/inc_con.php") ?>
<? include("functions.php") ?>
<? include("admin/includes/global_functions.php") ?>
<? include("admin/includes/queryFactory.php") ?>
<? include("inc_tag_header.php"); ?>
<body>

<?
$pageTitle = "Enlaces";
include("inc_header.php");


$links = getLinks();


?>

    <div class="divide80"></div>
<div class="container blog-left-img">
    <div class="row">
        <div class="col-sm-8">
            <? foreach($links as $item) { ?>

            <div class="results-box margin40" style="overflow: hidden">
                <h3><a target="_blank" href="<?= $item["link_url"] ?>"><?= $item["link_name"] ?></a></h3>
                <ul class="list-inline link-ul">
                    <li><?= $item["link_url"] ?></li>

                </ul>
                <p>
                    <div class="link-thumb" style="float: left; width: 150px; margin-right: 10px">
                    <img src="uploads/links/link_<?= $item["link_id"] ?>.jpg" alt="" style="max-width: 150px">
                    </div>
                    <?= $item["link_desc"] ?>
                </p>
            </div><!--result box-->
            <hr>
            <? }?>
        </div>

        <? include("sidebar.php"); ?>

    </div><!--row for blog post-->
</div><!--blog full main container-->
<div class="divide60"></div>
<? include("footer.php"); ?>
<? include("inc_scripts.php"); ?>


</body>
</html>