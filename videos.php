

<!DOCTYPE html>
<html lang="en">
<? include("admin/includes/inc_con.php") ?>
<? include("functions.php") ?>
<? include("admin/includes/global_functions.php") ?>
<? include("admin/includes/queryFactory.php") ?>
<? include("inc_tag_header.php"); ?>
<body>

<?
$pageTitle = "Videos";
include("inc_header.php");

// determine page (based on <_GET>)
$page = isset($_GET['page']) ? ((int) $_GET['page']) : 1;

$totalItems = getTotalVideos();
$amount = 8;
$start = ($page-1) * $amount;
$videos = getVideosPage($start, $amount);


?>

<div class="divide80"></div>
<div class="container blog-left-img">
    <div class="row">
        <div class="col-md-8">
            <? foreach($videos as $item) { ?>
            <div class="blog-post">

                <div class="row">
                    <div class="video-info">
                        <div class="col-md-6 margin20">
                            <a href="news.php?id=<? echo $item["news_id"] ?>">
                                <div class="item-img-wrap">
                                    <img src="uploads/videos/video_<?= $item["video_id"] ?>.jpg" class="img-responsive" alt="workimg">
                                </div>
                            </a><!--work link-->
                        </div>
                        <div class="col-md-6 margin20">

                            <h2><a href="videos.php?play=<? echo $item["video_id"] ?>"><? echo $item["video_title"] ?></a></h2>
                            <p>
                                <? echo $item["video_desc"] ?>
                            </p>
                            <p><a href="#" class="play-video-btn btn btn-theme-dark">Ver Video</a></p>
                        </div>
                    </div>
                    <div class="video-embed video-<? echo $item["video_id"] ?>" style="display: none">
                        <? echo $item["video_embed"] ?>
                    </div>
                </div>
            </div><!--blog post-->
            <? } ?>



            <?
            require_once 'Pagination.class.php';
            // instantiate; set current page; set number of records
            $pagination = (new Pagination());
            $pagination->setCurrent($page);
            $pagination->setTotal($totalItems);

            // grab rendered/parsed pagination markup
            $markup = $pagination->parse();

            ?>

            <!--                Pagination-->
            <div class="text-center">
                <?=  $markup; ?>
            </div>
            <!--End Pagination-->
        </div>

        <? include("sidebar.php"); ?>

    </div><!--row for blog post-->
</div><!--blog full main container-->
<div class="divide60"></div>
<? include("footer.php"); ?>
<? include("inc_scripts.php"); ?>


</body>
</html>