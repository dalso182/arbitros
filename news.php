<!DOCTYPE html>
<html lang="en">
<? include("admin/includes/inc_con.php") ?>
<? include("functions.php") ?>
<? include("admin/includes/global_functions.php") ?>
<? include("admin/includes/queryFactory.php") ?>
<? include("inc_tag_header.php"); ?>

<body>

<?
$pageTitle = "Noticia";
if(isset($_GET["id"])) {
    $id = $_GET["id"];
} else {
    $id = 1;
}


$item = getNews($id);
?>

<? include("inc_header.php"); ?>

<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="blog-post">
                <div>
                    <img src="uploads/news/news_<?= $item["news_id"] ?>.jpg" class="img-responsive" alt="workimg">

                </div>
                <ul class="list-inline post-detail">
                    <li>por <a href="#">arbitrosdecostarica.net</a></li>
                    <li><i class="fa fa-calendar"></i> <?= naturalDate($item["news_date"]); ?></li>
                </ul>
                <h2><?= $item["news_title"] ?></h2>
                <?= $item["news_content"] ?>
                <? if($item["news_video"]!="") {?>
                    <div class="news-video">
                        <?= $item["news_video"] ?>
                    </div>
                <? }?>
            </div><!--blog post-->

            <?
            $latestNews = getChildNewsPage(0, 6);
            include("latest.php"); ?>

        </div><!--col-->
        <?
        include("sidebar.php"); ?>

    </div><!--row for blog post-->
</div><!--blog full main container-->
<div class="divide60"></div>
<? include("footer.php"); ?>
<? include("inc_scripts.php"); ?>


</body>
</html>
