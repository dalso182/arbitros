<!DOCTYPE html>
<html lang="en">
<? include("admin/includes/inc_con.php") ?>
<? include("functions.php") ?>
<? include("admin/includes/global_functions.php") ?>
<? include("admin/includes/queryFactory.php") ?>
<? include("inc_tag_header.php"); ?>

<body>

<?
$pageTitle = "Noticia";
if(isset($_GET["id"])) {
    $id = $_GET["id"];
} else {
    $id = 1;
}

if($id== 9 || $id== 10 ) {
    $isColumn =true;
}

$item = getPage($id);
?>

<?
$pageTitle= $item["page_title"];
include("inc_header.php"); ?>

<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-md-6 margin20">
<!--            <h3 class="heading">--><?//= $item["page_title"] ?><!--</h3>-->
            <p>
                <?= $item["page_content"] ?>
            </p>

        </div>
        <div class="col-md-6 margin40">
            <img src="uploads/pages/page_<?= $item["page_id"] ?>.jpg" class="img-responsive" alt="">
        </div>
        <div class="col-md-6">
            <?
            $latestNews = getChildNewsPage(0, 6);
            $sliderCols = 6;
            include("latest.php"); ?>
        </div>
        <? //nclude("sidebar.php"); ?>

    </div><!--row for blog post-->
</div><!--blog full main container-->
<div class="divide60"></div>
<? include("footer.php"); ?>
<? include("inc_scripts.php"); ?>


</body>
</html>
