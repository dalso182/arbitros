<footer id="footer">
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center margin30">
                <div class="footer-col footer-3">
<!--                    <h3><img src="img/logo.png" alt=""/></h3>-->
                    <p>
                        Arbitrosdecostarica.net.  2015 &copy; Todos los derechos reservados.
                    </p>

                    <ul class="list-inline social-1 margin30">
                        <li><a href="https://www.facebook.com/pages/Arbitrosdecostaricanet/274644362604327"><i class="fa fa-facebook"></i></a></li>
                    </ul>

                </div>
            </div><!--footer col-->

        </div>

    </div>
    <p class="text-center"><span>Sitio Desarrollado por <a href="http://resume.dalsotec.com/" target="_blank">Diego Alvarez</a></span>
    </p>
</footer>