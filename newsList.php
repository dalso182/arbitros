

<!DOCTYPE html>
<html lang="en">
<? include("admin/includes/inc_con.php") ?>
<? include("functions.php") ?>
<? include("admin/includes/global_functions.php") ?>
<? include("admin/includes/queryFactory.php") ?>
<? include("inc_tag_header.php"); ?>
<body>

<?
$pageTitle = "Noticias";
include("inc_header.php");

// determine page (based on <_GET>)
$page = isset($_GET['page']) ? ((int) $_GET['page']) : 1;

$totalItems = getTotalNews();
$amount = 4;
$start = ($page-1) * $amount;
$news = getChildNewsPage($start, $amount);


?>

<div class="divide80"></div>
<div class="container blog-left-img">
    <div class="row">
        <div class="col-md-8">
            <? foreach($news as $item) { ?>
            <div class="blog-post">

                <div class="row">
                    <div class="col-md-6 margin20">
                        <a href="news.php?id=<? echo $item["news_id"] ?>">
                            <div class="item-img-wrap">
                                <img src="uploads/news/news_<?= $item["news_id"] ?>.jpg" class="img-responsive" alt="workimg">
                            </div>
                        </a><!--work link-->
                    </div>
                    <div class="col-md-6 margin20">
                        <ul class="list-inline post-detail">
                            <li>por <a href="#">arbitrosdecostarica.net</a></li>
                            <li><i class="fa fa-calendar"></i> <?= naturalDate($item["news_date"]); ?></li>
                        </ul>
                        <h2><a href="news.php?id=<? echo $item["news_id"] ?>"><? echo $item["news_title"];
                                if($item["news_video"]!="") echo " (Video)";
                                ?></a></h2>
                        <p>
                            <? echo $item["news_preview"] ?>
                        </p>
                        <p><a href="news.php?id=<? echo $item["news_id"] ?>" class="btn btn-theme-dark">Leer Más...</a></p>
                    </div>
                </div>
            </div><!--blog post-->
            <? } ?>



            <?
            require_once 'Pagination.class.php';
            // instantiate; set current page; set number of records
            $pagination = (new Pagination());
            $pagination->setCurrent($page);
            $pagination->setTotal($totalItems);

            // grab rendered/parsed pagination markup
            $markup = $pagination->parse();

            ?>

            <!--                Pagination-->
            <div class="text-center">
                <?=  $markup; ?>
            </div>
            <!--End Pagination-->
        </div>

        <? include("sidebar.php"); ?>

    </div><!--row for blog post-->
</div><!--blog full main container-->
<div class="divide60"></div>
<? include("footer.php"); ?>
<? include("inc_scripts.php"); ?>


</body>
</html>