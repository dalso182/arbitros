<? if(!isset($sliderCols)) $sliderCols = 8;
?>

<div class="container">
    <div class="row">
        <div class="col-md-<?=$sliderCols ?> margin40">
            <h3 class="heading">Ultimás Noticias</h3>

            <div id="news-carousel" class="owl-carousel owl-spaced">
                <? foreach($latestNews as $item) { ?>
                <div>
                    <a href="news.php?id=<? echo $item["news_id"] ?>">
                        <div class="item-img-wrap">
                            <img src="uploads/news/news_<?= $item["news_id"] ?>.jpg" class="img-responsive" alt="workimg">

                        </div>
                    </a><!--news link-->
                    <div class="news-desc">
                        <h4>
                            <a href="news.php?id=<? echo $item["news_id"] ?>"><? echo $item["news_title"];
                                if($item["news_video"]!="") echo " (Video)";
                                ?></a>
                        </h4>
                        <span><a class="btn btn border-black btn-xs" href="news.php?id=<? echo $item["news_id"] ?>">Leer Más...</a></span>
                    </div><!--news desc-->
                </div>
                <? } ?>

            </div>
        </div><!--col 7 end use for latest news owl carousel slide-->
    </div>
</div><!--latest news and why us container end-->