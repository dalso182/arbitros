<div class="col-md-3 col-md-offset-1">

    <div class="sidebar-box margin40">
        <h4>Escuchenos en: </h4>
        <p class="text-center"><a target="_blank" href="http://radioondatica.com/radio.html"><img src="img/ondatica.jpg" alt="" class=""></a></p>
        <p>
            Arbitrosdecostarica.net es colaborador activo de la emisora online del momento.
        </p>
    </div><!--sidebar-box-->

    <div class="sidebar-box margin40">
        <? $latestVideos = getVideosPage(0, 3);?>


        <h4>Videos</h4>
        <ul class="list-unstyled popular-post">
            <? foreach($latestVideos as $item) { ?>
            <li>
                <div class="popular-img">
                    <a href="#"> <img src="uploads/videos/video_<?= $item["video_id"] ?>.jpg" class="img-responsive" alt=""></a>
                </div>
                <div class="popular-desc">
                    <p> <a href="videos.php?id=<? echo $item["video_id"] ?>"><? echo $item["video_title"] ?></a></p>

                </div>
            </li>
            <? } ?>
        </ul>
    </div><!--sidebar-box-->

    <div class="sidebar-box margin40">
        <h4>Columnas</h4>
        <ul class="list-unstyled popular-post">
            <li>
                <div class="popular-img">
                    <a href="page.php?id=9"><img  src="img/columna0.jpg" class="img-responsive columna-img sm" alt=""></a>

                </div>
                <div class="popular-desc">
                    <h5> <a href="page.php?id=9">Marvin Ramirez</a></h5>
                    <h6>Editor</h6>
                </div>
            </li>
            <li>
                <div class="popular-img">
                    <a href="page.php?id=10"><img  src="img/columna1.jpg" class="img-responsive columna-img sm" alt=""></a>
                </div>
                <div class="popular-desc">
                    <h5> <a href="page.php?id=10">Freddy Burgos</a></h5>
                    <h6>Ex Arbitro FIFA</h6>
                </div>
            </li>
        </ul>
    </div><!--sidebar-box-->
    <div class="sidebar-box margin40">
        <h4>Patrocinadores</h4>
        <ul class="list-unstyled popular-post">
            <li>
                <a  href="https://www.facebook.com/transportesramirez506" target="_blank"><img src="img/pat1.jpg" alt=""></a>
            </li>
 
        </ul>
    </div><!--sidebar-box-->
</div><!--sidebar-col-->