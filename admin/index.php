
<!DOCTYPE html>
<?
$section="login";
include("includes/config.php");
?>
<html lang="en">
<? include("includes/inc_tag_head.php");?>

	
	
	<!-- BODY options, add following classes to body to change options

		1. 'sidebar-minified'     - Switch sidebar to minified version (width 50px)
		2. 'sidebar-hidden'		  - Hide sidebar
		3. 'rtl'				  - Switch to Right to Left Mode
		4. 'container'			  - Boxed layout
		5. 'static-sidebar'		  - Static Sidebar
		6. 'static-header'		  - Static Header
	-->
	
	<body class="login">
		

    <div class="container">
        <div class="row">
            <div class="login-box col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">

                <div class="header">
                    <?= $siteName?> Admin
                </div>

                <form id="form1" name="form1" method="post" action="validate.php">
                    <fieldset>
                        <div class="form-group first">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control input-lg" name="u" id="u" placeholder="Username or E-mail"/>
                            </div>
                        </div>

                        <div class="form-group last">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="password" class="form-control input-lg" name="p" id="p" placeholder="Password"/>
                            </div>
                        </div>
                        <br/>
                        <input name="enviar" type="submit" class="btn btn-primary col-xs-12" id="enviar" value="Login" />
                    </fieldset>

                </form>

            </div>
        </div><!--/row-->
    </div><!--/container-->


    <footer>
        <div class="row">
            <div class="col-sm-5">
                &copy; 2015 creativeLabs. <a href="http://bootstrapmaster.com">Admin Templates</a> by BootstrapMaster
            </div><!--/.col-->
            <div class="col-sm-7 text-right">
                Powered by: <a href="http://bootstrapmaster.com/demo/real/" alt="Bootstrap Admin Templates">Real Admin</a> | Based on Bootstrap 3.3.2 | Built with brix.io <a href="http://brix.io" alt="Brix.io - Bootstrap Builder">Brix.io</a>
            </div><!--/.col-->
        </div><!--/.row-->

    </footer>


    <? include("includes/inc_scripts.php");?>

</body>
</html>