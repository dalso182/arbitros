<?php include("includes/inc_session.php"); ?>

<!DOCTYPE html>
<html lang="en">
<?

include("includes/inc_tag_head.php")
?>
	<body >
		<!-- start: Header -->
        <? include("includes/inc_header.php");?>
		<!-- end: Header -->
		<!-- start: Main Menu -->
        <? include("includes/inc_main_nav.php");?>
		<!-- end: Main Menu -->

		<!-- start: Content -->
		<div class="main">
			<div class="row">
			    <div class="col-sm-10">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                <h2><strong>Company</strong> <small>Form</small></h2>
			            </div>
			            <div class="panel-body">
			                <div class="form-group">
			                    <label for="company">Company</label>
			                    <input type="text" class="form-control" id="company" placeholder="Enter your company name">
			                </div>

			                <div class="form-group">
			                    <label for="vat">VAT</label>
			                    <input type="text" class="form-control" id="vat" placeholder="PL1234567890">
			                </div>
			                <div class="form-group">
			                    <label for="street">Street</label>
			                    <input type="text" class="form-control" id="street" placeholder="Enter street name">
			                </div>
                            <div class="form-group">
                                <label class="control-label" for="date01">Form with Datepicker</label>
                                <div class="controls">
                                    <div class="input-group date col-sm-4">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control date-picker" id="date01" data-date-format="mm/dd/yyyy" />
                                    </div>
                                </div>
                            </div>
			                <div class="form-group">
			                    <label for="country">Country</label>
			                    <input type="text" class="form-control" id="country" placeholder="Country name">
			                </div>
			            </div>
			        </div>

			    </div><!--/col-->

			</div>
			<!--/.row-->
		</div>
		<!-- end: Content -->
		<footer>
			<div class="row">
				<div class="col-sm-5">
					&copy; 2015 creativeLabs. <a href="http://bootstrapmaster.com">Admin Templates</a> by BootstrapMaster
				</div><!--/.col-->

				<div class="col-sm-7 text-right">
					Powered by: <a href="http://bootstrapmaster.com/demo/real/" alt="Bootstrap Admin Templates">Real Admin</a> | Based on Bootstrap 3.3.2 | Built with brix.io <a href="http://brix.io" alt="Brix.io - Bootstrap Builder">Brix.io</a>
				</div><!--/.col-->	

			</div><!--/.row-->	
		</footer>
				<div class="modal fade" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<div class="modal-body">
						<p>Here settings can be configured...</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

        <? include("includes/inc_scripts.php");?>

	</body>
</html>