
<?
$table = "links";
$prefix= "link_";
$itemName = "Enlaces";
$subfolder="true";
$imagesPath= "../../uploads/links/";
$siteUrl= "/links.php?";
$key = $prefix."id";
$itemFields = array($prefix."id",$prefix."name",$prefix."url" ,$prefix."desc");

$tableHeaders = array("ID", "Nombre");
$tableFields = array($prefix."id", $prefix."name");


//$data = array("item_id"=>"1", "item_name"=>"test", "item_desc"=>"description", "item_price"=>"100");
$formElements =  array();

array_push($formElements, (object) array('name' => 'name', 'type' => 'text', 'label' => 'Nombre'));
array_push($formElements, (object) array('name' => 'desc', 'type' => 'textarea', 'label' => 'Descripción'));
array_push($formElements, (object) array('name' => 'url', 'type' => 'text', 'label' => 'URL'));
array_push($formElements, (object) array('name' => 'image', 'type' => 'image', 'label' => 'Imagen'));



