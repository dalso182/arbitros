<!DOCTYPE html>
<html lang="en">
<?
$itemName = "News";
if ($subfolder) {
  $includesPath = "../includes";
} else {
  $includesPath = "includes";
}

include("$includesPath/inc_tag_head.php");
?>


<body>
<!-- start: Layout Settings / remove this div from your project -->
<!-- start: Header -->
<? include("$includesPath/inc_header.php"); ?>
<!-- end: Header -->
<!-- start: Main Menu -->
<? include("$includesPath/inc_main_nav.php"); ?>

<!-- end: Main Menu -->

<!-- start: Content -->
<div class="main">

<?  include("alert.php") ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading" data-original-title>
          <h2><i class="fa fa-user"></i><span class="break"></span><?= $itemName ?></h2>
          <div class="filters hidden"><?= $filters ?></div>
        </div>
        <div class="panel-body">
          <table class="table table-striped table-bordered datatable ">
            <thead>
            <tr>
              <?php
              foreach ($tableHeaders as $header) {
                ?>
                <th><?= $header ?></th>
              <?php } ?>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $cont = 1;
            if (count($items) > 0) {
              foreach ($items as $item) {
                if (($cont++) % 2 == 0) $rowClass = "odd"; else $rowClass = "even";
                ?>
                <tr class="<?= $rowClass ?>">
                  <?php
                  foreach ($tableFields as $field) {
                    ?>
                    <td><?= $item["$field"] ?></td>
                  <?php } ?>

                  <td>
                    <a target="_blank" class="btn btn-success" href="<?= $siteUrl.$item["$key"] ?>">
                      <i class="fa fa-search-plus "></i>
                    </a>
                    <a class="btn btn-info" href="edit.php?id=<?= $item["$key"] ?>">
                      <i class="fa fa-edit "></i>
                    </a>
                    <a class="btn btn-danger" href="list.php?delete=<?= $item["$key"] ?>">
                      <i class="fa fa-trash-o "></i>
                    </a>
                  </td>
                </tr>
              <?php
              }
            } else {
              ?>
              <tr>
                <td colspan="3">No se encontraron <?= $itemName ?></td>
              </tr>
            <?php } ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/col-->

  </div>
  <!--/row-->
</div>
<!-- end: Content -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>Here settings can be configured...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<? include("$includesPath/inc_scripts.php"); ?>
</body>
</html>