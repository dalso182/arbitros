<div class="navbar" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><i class="icon-rocket"></i> <span>Admin</span></a>
    </div>
    <ul class="nav navbar-nav navbar-actions navbar-left">
        <li class="visible-md visible-lg"><a href="#" id="main-menu-toggle"><i class="fa fa-bars"></i></a></li>
        <li class="visible-xs visible-sm"><a href="#" id="sidebar-menu"><i class="fa fa-bars"></i></a></li>
    </ul>

    <ul class="nav navbar-nav navbar-right visible-md visible-lg">
        <li><a href="logout.php"><i class="icon-logout"></i></a></li>

    </ul>
</div>