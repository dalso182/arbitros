<?

if($subfolder) {
    $assetsPath="../assets";
} else {
    $assetsPath="assets";
}

?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Admin - <?= $siteName?>e">
    <meta name="keyword" content="">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= $assetsPath?>/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= $assetsPath?>/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= $assetsPath?>/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= $assetsPath?>/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?= $assetsPath?>/ico/favicon.png">

    <title>Admin - <?= $siteName?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?= $assetsPath?>/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-style">

    <!-- Remove following comment to add Right to Left Support or add class rtl to body -->
    <!-- <link href="<?= $assetsPath?>/css/bootstrap-rtl.min.css" rel="stylesheet"> -->

    <link href="<?= $assetsPath?>/css/jquery.mmenu.css" rel="stylesheet">
    <link href="<?= $assetsPath?>/css/simple-line-icons.css" rel="stylesheet">
    <link href="<?= $assetsPath?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= $assetsPath?>/css/jquery-te-1.4.0.css" rel="stylesheet">


    <!-- page css files -->
    <link href="<?= $assetsPath?>/plugins/jquery-ui/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <style>footer, #usage {
            display: none;
        }
    </style>

    <!-- page css files -->
    <link href="<?= $assetsPath?>/plugins/select2/css/select2.css" rel="stylesheet">
    <link href="<?= $assetsPath?>/plugins/select2/css/select2-bootstrap.css" rel="stylesheet">
    <link href="<?= $assetsPath?>/plugins/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= $assetsPath?>/css/style.min.css" rel="stylesheet" id="main-style">
    <link href="<?= $assetsPath?>/css/add-ons.min.css" rel="stylesheet">


    <!-- Remove following comment to add Right to Left Support or add class rtl to body -->
    <!-- <link href="<?= $assetsPath?>/css/style.rtl.min.css" rel="stylesheet"> -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>