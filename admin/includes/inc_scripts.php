<!-- start: JavaScript-->
<!--[if !IE]>-->

<script src="assets/js/jquery-2.1.1.min.js"></script>

<!--<![endif]-->

<!--[if IE]>

<script src="<?= $assetsPath?>/js/jquery-1.11.1.min.js"></script>

<![endif]-->

<!--[if !IE]>-->

<script type="text/javascript">
    window.jQuery || document.write("<script src='<?= $assetsPath?>/js/jquery-2.1.1.min.js'>"+"<"+"/script>");
</script>

<!--<![endif]-->

<!--[if IE]>

<script type="text/javascript">
    window.jQuery || document.write("<script src='<?= $assetsPath?>assets/js/jquery-1.11.1.min.js'>"+"<"+"/script>");
</script>

<![endif]-->
<script src="<?= $assetsPath?>/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= $assetsPath?>/js/bootstrap.min.js"></script>

<script src="<?= $assetsPath?>/plugins/jquery-ui/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?= $assetsPath?>/plugins/select2/js/select2.min.js"></script>
<script src="<?= $assetsPath?>/plugins/autosize/jquery.autosize.min.js"></script>
<script src="<?= $assetsPath?>/plugins/placeholder/jquery.placeholder.min.js"></script>
<script src="<?= $assetsPath?>/plugins/maskedinput/jquery.maskedinput.min.js"></script>
<script src="<?= $assetsPath?>/plugins/inputlimiter/js/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?= $assetsPath?>/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= $assetsPath?>/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?= $assetsPath?>/plugins/moment/moment.min.js"></script>
<script src="<?= $assetsPath?>/plugins/daterangepicker/js/daterangepicker.min.js"></script>
<script src="<?= $assetsPath?>/plugins/hotkeys/jquery.hotkeys.min.js"></script>
<script src="<?= $assetsPath?>/plugins/wysiwyg/bootstrap-wysiwyg.min.js"></script>
<script src="<?= $assetsPath?>/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>


<!-- theme scripts -->
<script src="<?= $assetsPath?>/plugins/pace/pace.min.js"></script>
<script src="<?= $assetsPath?>/js/jquery.mmenu.min.js"></script>
<script src="<?= $assetsPath?>/js/core.min.js"></script>

<!-- inline scripts related to this page -->
<script src="<?= $assetsPath?>/js/pages/form-elements.js"></script>
<script src="<?= $assetsPath?>/plugins/jquery-cookie/jquery.cookie.min.js"></script>
<script src="<?= $assetsPath?>/js/demo.min.js"></script>
<script src="<?= $assetsPath?>/js/mine.js"></script>

<!--http://jqueryte.com/-->
<script src="<?= $assetsPath?>/js/jquery-te-1.4.0.min.js"></script>
<script>
    $(".editor").jqte({
        "fsize":false,
        "format":false

    });
</script>
<!-- end: JavaScript-->