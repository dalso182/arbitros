


<div class="sidebar">

    <div class="sidebar-collapse">

        <div class="sidebar-header">

            <img src="<?= $assetsPath?>/img/avatar9.jpg">
            <h2>User</h2>
        </div>

        <div class="sidebar-menu">
            <ul class="nav nav-sidebar">
                <li>
                    <a href="#"><i class="icon-arrow-right"></i><span class="text"> Noticias</span> <span class="indicator"></span></a>
                    <ul>
                        <li><a href="../news/add.php"><i class="icon-plus"></i><span class="text"> Agregar</span></a></li>
                        <li><a href="../news/list.php"><i class="icon-pencil"></i><span class="text"> Editar/Borrar</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-arrow-right"></i><span class="text"> Videos</span> <span class="indicator"></span></a>
                    <ul>
                        <li><a href="../videos/add.php"><i class="icon-plus"></i><span class="text"> Agregar</span></a></li>
                        <li><a href="../videos/list.php"><i class="icon-pencil"></i><span class="text"> Editar/Borrar</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-arrow-right"></i><span class="text"> Enlaces</span> <span class="indicator"></span></a>
                    <ul>
                        <li><a href="../links/add.php"><i class="icon-plus"></i><span class="text"> Agregar</span></a></li>
                        <li><a href="../links/list.php"><i class="icon-pencil"></i><span class="text"> Editar/Borrar</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-arrow-right"></i><span class="text"> Paginas</span> <span class="indicator"></span></a>
                    <ul>
                        <? $pages = getPages();
                        foreach($pages as $page) {?>
                        <li><a href="../pages/edit.php?id=<?= $page["page_id"]?>" style="font-size: 11px;"><i class="icon-pencil"></i><span class="text"> <?= $page["page_title"]?></span></a></li>
                        <? }?>
                    </ul>
                </li>


            </ul>
        </div>
    </div>
    <div class="sidebar-footer">
        <ul class="sidebar-actions" style="text-align: right; padding-right: 2%">
            <li class="action">
                <div class="btn-group dropup">
                    <button type="button" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-power"></i><span>Salir</span>
                    </button>
        </ul>

    <!--    <ul class="sidebar-terms">-->
    <!--        <li><a href="form-elements.html#">Terms</a></li>-->
    <!--        <li><a href="form-elements.html#">Privacy</a></li>-->
    <!--        <li><a href="form-elements.html#">Help</a></li>-->
    <!--        <li><a href="form-elements.html#">About</a></li>-->
    <!--    </ul>-->

    </div>
</div>