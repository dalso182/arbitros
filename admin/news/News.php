
<?
$table = "news";
$prefix= "news_";
$itemName = "Noticias";
$subfolder="true";
$imagesPath= "../../uploads/news/";
$siteUrl= "/news.php?id=";
$key = $prefix."id";
$itemFields = array($prefix."id",$prefix."cat", $prefix."title", $prefix."content", $prefix."date", $prefix."video", $prefix."preview");

$tableHeaders = array("ID", "Titulo", "Fecha" );
$tableFields = array($prefix."id", $prefix."title", $prefix."date");


//$data = array("item_id"=>"1", "item_name"=>"test", "item_desc"=>"description", "item_price"=>"100");
$formElements =  array();

array_push($formElements, (object) array('name' => 'title', 'type' => 'text', 'label' => 'Título'));
array_push($formElements, (object) array('name' => 'preview', 'type' => 'textarea', 'label' => 'Descripción'));
array_push($formElements, (object) array('name' => 'content', 'type' => 'editor', 'label' => 'Contenido'));
array_push($formElements, (object) array('name' => 'date', 'type' => 'date', 'label' => 'Fecha'));
array_push($formElements, (object) array('name' => 'video', 'type' => 'video', 'label' => 'Video'));
array_push($formElements, (object) array('name' => 'image', 'type' => 'image', 'label' => 'Imagen'));


