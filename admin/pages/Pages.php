
<?
$table = "pages";
$prefix= "page_";
$itemName = "Paginas";
$subfolder="true";
$imagesPath= "../../uploads/pages/";
$key = $prefix."id";
$itemFields = array($prefix."id",$prefix."title", $prefix."content");

$tableHeaders = array("ID", "Titulo");
$tableFields = array($prefix."id", $prefix."title");


//$data = array("item_id"=>"1", "item_name"=>"test", "item_desc"=>"description", "item_price"=>"100");
$formElements =  array();
array_push($formElements, (object) array('name' => 'title', 'type' => 'text', 'label' => 'Título'));
array_push($formElements, (object) array('name' => 'content', 'type' => 'editor', 'label' => 'Contenido'));
array_push($formElements, (object) array('name' => 'image', 'type' => 'image', 'label' => 'Imagen'));


