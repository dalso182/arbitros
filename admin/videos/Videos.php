
<?
$table = "videos";
$prefix= "video_";
$itemName = "Videos";
$subfolder="true";
$imagesPath= "../../uploads/videos/";
$siteUrl= "/video.php?id=";
$key = $prefix."id";
$itemFields = array($prefix."id",$prefix."title", $prefix."desc", $prefix."embed");

$tableHeaders = array("ID", "Titulo", "Desc" );
$tableFields = array($prefix."id", $prefix."title", $prefix."desc");


//$data = array("item_id"=>"1", "item_name"=>"test", "item_desc"=>"description", "item_price"=>"100");
$formElements =  array();

array_push($formElements, (object) array('name' => 'title', 'type' => 'text', 'label' => 'Título'));
array_push($formElements, (object) array('name' => 'desc', 'type' => 'textarea', 'label' => 'Descripción'));
array_push($formElements, (object) array('name' => 'embed', 'type' => 'video', 'label' => 'Video'));
array_push($formElements, (object) array('name' => 'image', 'type' => 'image', 'label' => 'Thumbnail'));



