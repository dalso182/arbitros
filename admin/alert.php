<?
if (strpos($msg,"Error") !== false) {
    $msgClass =  "alert-danger";
} else {
    $msgClass =  "alert-success";
}
?>

<? if(isset($msg)) { ?>
    <div class="alert <?= $msgClass; ?>"><?= $msg?></div>
<? }?>