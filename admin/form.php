<?php include("includes/inc_session.php"); ?>

<!DOCTYPE html>
<html lang="en">
<?



include("includes/inc_tag_head.php")
?>

<body>
<!-- start: Header -->
<? include("includes/inc_header.php"); ?>
<!-- end: Header -->
<!-- start: Main Menu -->
<? include("includes/inc_main_nav.php"); ?>

<!-- end: Main Menu -->

<!-- start: Content -->
<div class="main">

    <?
    //variable to check if form is in edit mode
    if($formAction == "edit"){
      $editing = true;
    } else $editing = false;

    include("alert.php")
    ?>

<? if(!isset($msg)) { ?>
  <div class="row">

    <div class="col-sm-10">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h2><strong><?= $itemName?></strong>
            <small><?= $formAction ?></small>
          </h2>
        </div>
        <form method="post" action="<?php echo $formAction ?>.php" enctype="multipart/form-data">
          <div class="panel-body">

              <?
              if($table=="news")
              include("cat-dropdown.php");
              ?>

            <?php foreach ($formElements as $el) { ?>

              <? if ($el->type == "text") { ?>
                <div class="form-group">
                  <label for="<?= $prefix . $el->name ?>"><?= $el->label ?></label>
                  <input type="text" class="form-control" id="<?= $prefix . $el->name ?>"
                         name="<?= $prefix.$el->name ?>" placeholder="<?= $el->label ?>" value="<?= $item[$prefix.$el->name] ?>">
                </div>
              <?php } ?>

              <? //////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
              <? if ($el->type == "textarea") { ?>
                <div class="form-group">
                  <label class="control-label" for="<?= $prefix . $el->name ?>"><?= $el->label ?></label>

                  <div class="">
                    <textarea id="<?= $prefix . $el->name ?>" name="<?= $prefix . $el->name ?>" rows="9"
                              class="form-control" placeholder="<?= $el->label ?>"
                              style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 194px;"><?= $item[$prefix.$el->name] ?></textarea>
                  </div>
                </div>
              <?php } ?>

              <? //////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
                <? if ($el->type == "editor") { ?>
                    <div class="form-group">
                        <label class="control-label" for="<?= $prefix . $el->name ?>"><?= $el->label ?></label>

                        <div class="">
                            <textarea id="<?= $prefix . $el->name ?>" name="<?= $prefix . $el->name ?>" rows="9"
                                      class="form-control editor" placeholder="<?= $el->label ?>"
                                      style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 194px;"><?= $item[$prefix.$el->name] ?></textarea>
                        </div>
                    </div>
                <?php } ?>

              <? //////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
              <? if ($el->type == "date") { ?>
                <div class="form-group">
                  <label class="control-label" for="<?= $prefix . $el->name ?>"><?= $el->label ?></label>

                  <div class="controls">
                    <div class="input-group date col-sm-4">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      <input type="text" class="form-control date-picker" id="<?= $prefix . $el->name ?>"
                             name="<?= $prefix . $el->name ?>" data-date-format="yyyy-mm-dd" value="<?= $item[$prefix.$el->name] ?>"/>
                    </div>
                  </div>
                </div>
              <?php } ?>

              <? //////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
              <? if ($el->type == "video") { ?>
                <div class="form-group">
                  <label for="<?= $prefix . $el->name ?>"><?= $el->label ?></label>

                  <input type="text" class="form-control" id="<?= $prefix . $el->name ?>"
                         name="<?= $prefix.$el->name ?>" placeholder="<?= $el->label ?>" value="<?= htmlentities($item[$prefix.$el->name]) ?>">

                  <div class="preview">
                    <? if($editing) {
                      echo $item[$prefix.$el->name] ;
                    }
                    ?>
                  </div>
                </div>
              <?php } ?>
                <? //////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
                <? if ($el->type == "image") { ?>
                    <div class="form-group">
                        <label for="<?= $prefix . $el->name ?>"><?= $el->label ?></label>

                        <input type="file" class="form-control" id="<?= $prefix . $el->name ?>"
                               name="<?= $prefix.$el->name ?>" placeholder="<?= $el->label ?>" value="">

                        <div class="preview">
                            <?

                            if($editing) { ?>
                                <img src="<? echo $imagesPath.$prefix.$item[$key].".jpg" ?>" alt=""/>
                            <?  }
                            ?>
                        </div>
                    </div>
                <?php } ?>

            <?php } ?>

          </div>
          <div class="panel-footer">
            <?php
            if($editing){
              ?>
              <input type="hidden" name="editCode" id="editCode" value="<?= $item[$key]?>" />
            <?php } ?>
            <button type="submit" name="send" id="send" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
            <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
          </div>
        </form>
      </div>

    </div>
    <!--/col-->

  </div>

  <!--/.row-->
    <? } else { //if(!isset($msg)  ?>
    <div class="row">

    <div class="col-sm-10">
        <a href="add.php" class="btn btn-success">Agregar</a>
        <a href="list.php" class="btn btn-success">Ver Listado</a>
    </div>
    </div>

    <? }?>
</div>
<!-- end: Content -->

<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>Here settings can be configured...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<? include("includes/inc_scripts.php"); ?>

</body>
</html>