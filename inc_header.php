



<div id="header-top" class="hidden-xs">
    <div class="container">
        <div class="top-bar">
            <div class="pull-left sample-1right">

            </div>
            <div class="pull-right">
                <ul class="list-inline top-social">
                    <li>Encuentrenos en:</li>
                    <li><a target="_blank" href="https://www.facebook.com/pages/Arbitrosdecostaricanet/274644362604327"><i class="fa fa-facebook"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div><!--top bar end hidden in small devices-->
<!--navigation -->
<!-- Static navbar -->
<div class="navbar navbar-default navbar-static-top yamm sticky" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo-text.png" alt="Arbitrosdecostarica"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="page.php?id=1" class="" >Historia <i class="fa "></i></a>
                </li>
                <!--menu home li end here-->
                <li class="">
                    <a href="newsList.php" class=" " >Noticias <i class="fa "></i></a>
                </li>
                <!--menu Portfolio li end here-->
                <li class=" ">
                    <a href="videos.php" class="" ">Videos <i class="fa "></i></a>

                </li>
                <!--menu blog li end here-->
                <li class="dropdown">
                    <a href="links.php" class="">Enlaces <i class="fa "></i></a>
                </li>
                <!--menu pages li end here-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Arbitraje en Costa Rica <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="page.php?id=3">Entrevista de la Semana</a></li>
                        <li><a href="page.php?id=5">Nuestros Referentes</a></li>
                        <li><a href="page.php?id=6">Comisión de Arbitraje</a></li>
                        <li><a href="page.php?id=7">Sub Comisión de Arbitraje</a></li>
                        <li><a href="page.php?id=8">Asociaciones Arbitrales</a></li>
                    </ul>
                </li> <!--menu Shop li end here-->


                <li class="dropdown " data-animate="animated fadeInUp" style="z-index:500;">
                    <a href="#" class="dropdown-toggle " data-toggle="dropdown"><i class="fa fa-search"></i></a>
                    <ul class="dropdown-menu search-dropdown animated fadeInUp">
                        <li id="dropdownForm">
                            <div class="dropdown-form">
                                <form class=" form-inline" action="search.php" method="post">
                                    <div class="input-group">
                                        <input type="text" name="searchWords" class="form-control" placeholder="Buscar Noticias...">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-theme-bg" type="submit">Ir!</button>
                                                </span>
                                    </div><!--input group-->
                                </form><!--form-->
                            </div><!--.dropdown form-->
                        </li><!--.drop form search-->
                    </ul><!--.drop menu-->
                </li> <!--menu search li end here-->
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--container-->
</div><!--navbar-default-->
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <?if($isColumn) {
                $pageTitle = "Columna ".$pageTitle;
                ?>
<!--                <div class="col-sm-1">-->
<!---->
<!--                </div>-->
            <? }?>

            <div class="col-sm-6">
                <h4><?= $pageTitle; ?></h4>
            </div>
<!--            <div class="col-sm-6 hidden-xs text-right">-->
<!--                <ol class="breadcrumb">-->
<!--                    <li><a href="index.html">Home</a></li>-->
<!--                    <li>blog-sidebar</li>-->
<!--                </ol>-->
<!--            </div>-->
        </div>
    </div>
</div><!--breadcrumbs-->