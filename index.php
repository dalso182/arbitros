<!DOCTYPE html>
<html lang="en">
<? include("admin/includes/inc_con.php") ?>
<? include("functions.php") ?>
<? include("admin/includes/global_functions.php") ?>
<? include("admin/includes/queryFactory.php") ?>
<? include("inc_tag_header.php"); ?>

<body>

<?
 $pageTitle = "Inicio";
?>

<? include("inc_header.php");
$latestNews = getChildNewsPage(0, 7);
$last = $latestNews[0];
?>
<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="blog-post">
                <a href="#">
                    <div class="item-img-wrap">
                        <img src="uploads/news/news_<?= $last["news_id"] ?>.jpg" class="img-responsive" alt="workimg">
                    </div>
                </a><!--work link-->
                <ul class="list-inline post-detail">
                    <li>by <a href="#">arbitrosdecostarica.net</a></li>
                    <li><i class="fa fa-calendar"></i> <?= naturalDate($last["news_date"]); ?></li>
<!--                    <li><i class="fa fa-comment"></i> <a href="#">6 Comments</a></li>-->
                </ul>
                <h2><a href="news.php?id=<? echo $last["news_id"] ?>"><? echo $last["news_title"];
                        if($last["news_video"]!="") echo " (Video)";
                        ?></a></h2>
                <p>
                    <? echo $last["news_preview"] ?>
                </p>
                <p><a href="news.php?id=<? echo $last["news_id"] ?>" class="btn btn-theme-dark">Leer...</a></p>
            </div><!--blog post-->
            <?
             array_shift($latestNews);
            include("latest.php");
            ?>
        </div><!--col-->
        <? include("sidebar.php"); ?>

    </div><!--row for blog post-->
</div><!--blog full main container-->
<div class="divide60"></div>
<? include("footer.php"); ?>
<? include("inc_scripts.php"); ?>


</body>
</html>
